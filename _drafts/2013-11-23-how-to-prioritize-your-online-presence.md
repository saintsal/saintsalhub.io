---
layout: post
title:  "How To Prioritize Your Online Presence"
date:   2013-11-23 15:13:33
tags:   Advice
---

To be effective with most things, you must spend your time where it matters most.  Traditional design practices are drawn out and allow irrelevant details to take precedence over entrepreneurial values, like speed to market. Why is Iterative Design better? Consider this simple example.

Recently, I was helping my friend  set up her website. She’s an artist and illustrator, and was resisting the suggestion that the website design is far from her top priority, even though the design has been holding up the entire process until now. She has a unique style and a very fun, human way of communicating. But as she puts it, so far her great work and ability has been kept “hidden under the bed.”

I’ve seen this happen with all kinds of freelancers and service businesses. We let our professional perfectionism overturn our business priorities. From our current perspective, we feel the only way for our website to communicate the quality of our work is for the website to be an example of our work. A writer toils over the website copy, the photographer over the site imagery, and meanwhile there’s no site, and no new clients!

So even with such a clear goal of gaining clients as quickly as possible, why is it so hard to prioritize? Why is it hard to single out the priorities that get us to our goals the fastest?

To dependably identify the highest leverage activities, there’s are established ways of thinking:
<ol>
	<li>Take baby steps. Start with the bare minimum you need to get your first customer. It may be that a more hastily designed site might not land as many customers, but it will land some. More imporantly, it will land them sooner. Consider that once you have client #1, you’ll have a bit more cash and time to invest in your site.</li>
	<li>Begin at the end and work backwards.  Imagine when you have loads of clients, and an established reputation. What will be the most important aspects of your website? Chances are they will be your portfolio and the description of your methods. Basically, the things that communicate your expertise. Remember, it’s not our websites that represent us, it’s our work that represents us.  When you’re busy with client work, imagine how you will communicate your expertise very quickly by keeping things simple.</li>
	<li>Consider the customers perspective.  Ask any freelancer who keeps website stats – people go straight to two places: About Us and Portfolio, and some move on to contact. Once someone is on a site, all other content is basically ignored.</li>
</ol>
I suggested to my friend, as I do for many startup businesses, that a blog with a template design actually offers a greater payback at a lower cost. A blog allows you to focus on the getting the message out. The website’s measure of success is the number of clients she wins, whch depends on how quickly it gets them. Promotion and content are the key factors here.  Within a few hours, she found a suitable template and had begun populating it with her work.

If you’re having trouble getting your website or social media started, Stay Connected has an <a title="Website and Social Media Quick Start" href="http://www.stayconnected.me/marketing-agency-services/online-quick-start">Online Quick Start service</a>, which can get you online within days. It’s a high leverage service that stays focused on your strategic goals, and uses high quality but  free, open-source resources to make progress quickly.
