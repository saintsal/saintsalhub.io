---
layout: post
title:  "Tweets from the week 2010-01-25"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>@<a href="http://twitter.com/4ip">4ip</a> <a href="http://submit.4ip.org.uk/" rel="nofollow">http://submit.4ip.org.uk/</a> is down and I have a great idea for you! <a href="http://twitter.com/saintsal/statuses/7993603934">#</a></li>
	<li>What awesomeness could you possibly launch with just 10 hrs/week? <a href="http://is.gd/6K5KA" rel="nofollow">http://is.gd/6K5KA</a> /by @<a href="http://twitter.com/ewanmcintosh">ewanmcintosh</a> @<a href="http://twitter.com/pleaseenjoy">pleaseenjoy</a> #<a href="http://search.twitter.com/search?q=%23rework">rework</a> #<a href="http://search.twitter.com/search?q=%23leanstartup">leanstartup</a> <a href="http://twitter.com/saintsal/statuses/8030976400">#</a></li>
	<li>@<a href="http://twitter.com/kevnd">kevnd</a> Thanks! I think awesomeness is a commonly-understood virtue. And a handy standard when deciding how to cut scope down. <a href="http://twitter.com/kevnd/statuses/8031466682">in reply to kevnd</a> <a href="http://twitter.com/saintsal/statuses/8031900180">#</a></li>
	<li>@<a href="http://twitter.com/pantheon_drupal">pantheon_drupal</a> Ever had a problem with drupal_goto()? I&#39;ve copied a site to a mercury image &amp; it&#39;s ignoring the header(&quot;Location... line. <a href="http://twitter.com/saintsal/statuses/8035706790">#</a></li>
	<li>Just arrived at #140conf.  Is being late fashionable on the real-time web? (@ Ablemarle Consulting w/ 6 others) <a href="http://4sq.com/5ETYiH" rel="nofollow">http://4sq.com/5ETYiH</a> <a href="http://twitter.com/saintsal/statuses/8039339706">#</a></li>
	<li>Cross-network noise: a result of low demand for filters or poor community design? @<a href="http://twitter.com/imajes">imajes</a> @<a href="http://twitter.com/jobsworth">jobsworth</a> #<a href="http://search.twitter.com/search?q=%23140conf">140conf</a> <a href="http://twitter.com/saintsal/statuses/8039743070">#</a></li>
	<li>RT @farhan: 3 billion ppl in India and China coming online about to change the meaning of privacy according to @<a href="http://twitter.com/jobsworth">jobsworth</a> #<a href="http://search.twitter.com/search?q=%23140Conf">140Conf</a> <a href="http://twitter.com/saintsal/statuses/8039789177">#</a></li>
	<li>@<a href="http://twitter.com/priyal">priyal</a> was fun chatting with you guys.  Check out @<a href="http://twitter.com/saul">saul</a> and @<a href="http://twitter.com/thepeopletweet">thepeopletweet</a> and maybe see at an event of theirs. <a href="http://twitter.com/saintsal/statuses/8068253115">#</a></li>
	<li>@<a href="http://twitter.com/smidgn">smidgn</a> meet @tav, @<a href="http://twitter.com/tav">tav</a> meet @<a href="http://twitter.com/smidgn">smidgn</a> - both of you are builders in the trust network space. <a href="http://twitter.com/saintsal/statuses/8068271505">#</a></li>
	<li>@<a href="http://twitter.com/fundingpartners">fundingpartners</a> Nice to meet you last night.  Thanks for the advice.  I didn&#39;t re <a href="http://twitter.com/saintsal/statuses/8076673945">#</a></li>
	<li>@<a href="http://twitter.com/fundingpartners">fundingpartners</a> I didn&#39;t realize you&#39;re so connected to @s4stv. I&#39;ve approached them on a different project. <a href="http://twitter.com/saintsal/statuses/8076758437">#</a></li>
	<li>@<a href="http://twitter.com/tomball2">tomball2</a> Good to meet you at #140conf. Tom at Ablestoke told me about Congac - communicate anything in 10 minutes! Have you met @davegray? <a href="http://twitter.com/saintsal/statuses/8077206927">#</a></li>
	<li>@<a href="http://twitter.com/tommorris">tommorris</a> Of course, they know a much more efficient way. Invade. <a href="http://twitter.com/tommorris/statuses/8077253090">in reply to tommorris</a> <a href="http://twitter.com/saintsal/statuses/8077309268">#</a></li>
	<li>@<a href="http://twitter.com/virginmedia">virginmedia</a> Your IP (82.35.68.153) is blacklisted on zen.spamhaus.org, stopping me from sending email. Please sort this out. <a href="http://twitter.com/saintsal/statuses/8090414491">#</a></li>
	<li>FT: Why the best investors build checklists.  (@<a href="http://twitter.com/fundingpartners">fundingpartners</a>  this reminded me of our conversation.) <a href="http://is.gd/6WaOO" rel="nofollow">http://is.gd/6WaOO</a> <a href="http://twitter.com/saintsal/statuses/8152059111">#</a></li>
	<li>Mag+, a concept video on the future of magazines and handheld content. <a href="http://is.gd/70Osv" rel="nofollow">http://is.gd/70Osv</a>  /via @<a href="http://twitter.com/fergb">fergb</a> #<a href="http://search.twitter.com/search?q=%23analogweb">analogweb</a> <a href="http://twitter.com/saintsal/statuses/8199298594">#</a></li>
</ul>

