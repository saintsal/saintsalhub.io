---
layout: post
title:  "Tweets from the week 2010-01-11"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>The F-Word (show) is a lesson in #customerdevelopment. How many chefs don&#39;t follow up with their customers yet assume they are satisfied? <a href="http://twitter.com/saintsal/statuses/7416187806">#</a></li>
	<li>@<a href="http://twitter.com/IATV">IATV</a> That&#39;s great! @<a href="http://twitter.com/nickycast">nickycast</a> &amp; I were just in Berlin and loved it! I&#39;ve joined Ixda Berlin so we can plan a return around a UX event. <a href="http://twitter.com/IATV/statuses/7422394989">in reply to IATV</a> <a href="http://twitter.com/saintsal/statuses/7423920193">#</a></li>
	<li>@<a href="http://twitter.com/dokumori">dokumori</a> How&#39;s your Drupal performance investigation going? Have you seen Pantheon?  <a href="http://is.gd/5O5Ha" rel="nofollow">http://is.gd/5O5Ha</a> <a href="http://twitter.com/saintsal/statuses/7441571932">#</a></li>
	<li>@<a href="http://twitter.com/smarimc">smarimc</a> @<a href="http://twitter.com/Olasofia">Olasofia</a> The only way of determining the limits of the possible is to venture into the impossible. - Arthur C Clarke <a href="http://twitter.com/saintsal/statuses/7441824098">#</a></li>
	<li>No offense Twitter, but I&#39;m seeing much greater potential for real-time learning at <a href="http://vark.com" rel="nofollow">http://vark.com</a> <a href="http://twitter.com/saintsal/statuses/7460580930">#</a></li>
	<li>Entrepreneurs, keep your eyes on this apt match-up. Business Model Generation meets Lean Startup. /via @<a href="http://twitter.com/ericries">ericries</a> <a href="http://ff.im/-dUFG2" rel="nofollow">http://ff.im/-dUFG2</a> <a href="http://twitter.com/saintsal/statuses/7463477067">#</a></li>
	<li>IPhone killer for under 30 Euro: <a href="http://nerds.computernotizen.de/2010/01/07/iphone-killer-fur-unter-20-euro/" rel="nofollow">http://nerds.computernotizen.de/2010/01/07/iphone-killer-fur-unter-20-euro/</a> /via @<a href="http://twitter.com/codepo8">codepo8</a> <a href="http://twitter.com/saintsal/statuses/7477131213">#</a></li>
	<li>I&#39;d like a &quot;Watch It Later&quot; app to queue audio/video on my iphone.  Any ideas? Do I have to roll my own? <a href="http://twitter.com/saintsal/statuses/7531903136">#</a></li>
	<li>@<a href="http://twitter.com/fivesectest">fivesectest</a> It&#39;s fun to take your tests. Any advice or posts on how to best learn from click tests and memory tests? <a href="http://twitter.com/saintsal/statuses/7534791921">#</a></li>
	<li>With a multi-sided market should one validate all sides before crossing the chasm? #<a href="http://search.twitter.com/search?q=%23leanstartup">leanstartup</a> #<a href="http://search.twitter.com/search?q=%23bmgen">bmgen</a> /cc @<a href="http://twitter.com/ericries">ericries</a> @<a href="http://twitter.com/tor">tor</a> @<a href="http://twitter.com/business_design">business_design</a> <a href="http://twitter.com/saintsal/statuses/7555187369">#</a></li>
	<li>A sneak peak at the illustrations in #Rework, @37Signals&#39; new book. #<a href="http://search.twitter.com/search?q=%23custdev">custdev</a> #<a href="http://search.twitter.com/search?q=%23vizthink">vizthink</a> <a href="http://ff.im/-e3feK" rel="nofollow">http://ff.im/-e3feK</a> <a href="http://twitter.com/saintsal/statuses/7559145336">#</a></li>
</ul>

