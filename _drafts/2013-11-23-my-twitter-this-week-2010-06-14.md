---
layout: post
title:  "My Twitter this week: 2010-06-14"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>Video: Complexity + Humanity. Or how I learned to stop assuming and love pattern recognition. /via @<a href="http://twitter.com/nickycast">nickycast</a> <a href="http://tumblr.com/xhnbapz5j" rel="nofollow">http://tumblr.com/xhnbapz5j</a> <a href="http://twitter.com/saintsal/statuses/15838668971">#</a></li>
	<li>Can anyone please recommend someone for group CiviCRM training in London? <a href="http://twitter.com/saintsal/statuses/15842215964">#</a></li>
	<li>@<a href="http://twitter.com/timruffles">timruffles</a> How&#39;d the #<a href="http://search.twitter.com/search?q=%23custdev">custdev</a> cold calls go? <a href="http://twitter.com/timruffles/statuses/15770902986">in reply to timruffles</a> <a href="http://twitter.com/saintsal/statuses/15842261450">#</a></li>
	<li>@<a href="http://twitter.com/damiensaunders">damiensaunders</a> Thanks for the invite &amp; congrats on Startup Weekend! I&#39;ll try to pop by today. <a href="http://twitter.com/damiensaunders/statuses/15709729231">in reply to damiensaunders</a> <a href="http://twitter.com/saintsal/statuses/15845463677">#</a></li>
</ul>

