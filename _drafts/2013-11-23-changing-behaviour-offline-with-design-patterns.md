---
layout: post
title:  "Changing Behaviour Offline with Design Patterns"
date:   2013-11-23 15:13:33
tags:   
---

<p>The web is expanding to the analog world via wearable computing, tangible &amp; ambient media, digital signage, smart materials and augmented reality. For SXSW, I&#8217;m putting together <a href="http://panelpicker.sxsw.com/ideas/view/4649">a panel of technologists and marketers</a> to analyse  emerging design patterns and existing practices, and demonstrate how to drive behavioural change in the real world. (<a href="http://panelpicker.sxsw.com/ideas/view/4649">Your vote here would be helpful &#8211; so please give us a thumbs up!</a>)<a href="http://panelpicker.sxsw.com/ideas/view/4649"></a></p>
<p>If you work with ubiquitous computing, digital signage, out-of-home media, or any variant of the Analog Web, you&#8217;ll learn techniques and design patterns that will help with your work now, and get you thinking about where our work will change in the future.</p>
<p>We&#8217;ll be covering the following questions:</p>
<dl>
<dd>
<ol>
<li> How does our environment affect our memories?</li>
<li> How can design patterns improve traffic control, yield management and peak load issues in the real world?</li>
<li> Which social media marketing techniques will be applicable in mass advertising campaigns?</li>
<li> How will ambient media &amp; time-shifting behaviour change the requirements for creative briefs? How can social cues be shifted through time and space to drive collective behaviour?</li>
<li> How will face recognition make digital signage campaigns more measurable than online marketing?</li>
<li> How are consumers&#8217; emotional relationships with objects changing, and how can we create stronger calls to action using computer-identifiable objects?</li>
<li> How have user-centered design principles been used to change our attitudes and beliefs?</li>
<li> How can environmental control have a greater impact on people&#8217;s behaviour than overt messaging?</li>
<li> As public spaces expose APIs and people gain greater control of their environments, how will public landscapes change?</li>
<li> How do we prioritise? Which information presentation techniques drive the most behavioural change? How can we better determine which behaviours we want to change?</li>
</ol>
</dd>
</dl>
<p>We need your votes to make this happen, so if this topic interests you, <a href="http://panelpicker.sxsw.com/ideas/view/4649">please vote for us</a>.</p>

