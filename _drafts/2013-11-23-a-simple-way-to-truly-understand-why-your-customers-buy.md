---
layout: post
title:  "The question that almost always lifts conversion, or points to a pivot."
date:   2013-11-23 15:13:33
tags:   Customer-Development decisions
---

Ask a founder to describe what their startup does. Then ask their customer.Think they'll say the same thing? Probably not. How badly to you think this slows down the founder?

When I co-founded a business providing business centres to hotels, we were convinced that the new revenue stream was the main reason hotels bought from us. But it turned out to be the fact that at busy check-in times, the front desk gets overwhelmed with guests asking them to print their boarding passes. (This was about 10 years ago.) Learning this unleashed our sales.

<a href="http://www.flickr.com/photos/60615284@N00/3722204564" target="_blank"><img class="zemanta-img-inserted zemanta-img-configured " title="Johnson's USB key" alt="Johnson's USB key" src="http://farm4.static.flickr.com/3023/3722204564_47522d88de_m.jpg" width="108" height="144" /></a>

This was a happy accident - we heard how front-desk managers described us to the general managers. <em>"No more dealing with USB keys if we go with these guys!!"  </em>In retrospect, we should have learned this sooner.

Now, I always ask my customers to describe my business back to me. I usually do this in a satisfaction survey using 3 questions.
<blockquote>How likely would you be to recommend us?

Who would you recommend us to?

<strong>Imagine you're speaking to them.[1] How would you describe us to them?</strong></blockquote>
The first two questions put the customer in the right mindset for the third.  I want to know how my customers describe my business to each other. I want to know their exact words, their vernacular, and the context they put me in.

<a href="http://www.flickr.com/photos/29468339@N02/4597412009" target="_blank"><img class="zemanta-img-inserted zemanta-img-configured " title="Love ? I love love love you." alt="Love ? I love love love you." src="http://farm5.static.flickr.com/4023/4597412009_cc9138f1b3_m.jpg" width="144" height="108" /></a> 

This makes sure I have a well-aligned value proposition, and the right product direction.

I test their phrases in my advertising, headlines and copy. It almost always gets a significant conversion uplift. Even better, it tells me what features are (and aren't) important to my customers and how they'll be used.

In surveys, 1 in 20 of the responses are useful. (Often the description is just superlatives like "really great!")

But sometimes, you can get this highly actionable information with just a few emails.

<strong>So email 3 of your most evangelical customers now. </strong>No really! You'll be surprised what you learn. 

And if it doesn't expose a big opportunity, it'll likely still boost your conversion. I've even preformatted the email for you so you just need to <a href="mailto:name@customer.com?subject=Quick questions&amp;body=Hi ,%0D%0ACould you please take 2 minutes to answer some questions for me? I'm trying to get a better understanding of how we help our customers, particularly why we're helpful and what we do from your context.%0D%0A%0D%0A How likely would you be to recommend us?%0D%0A%0D%0A Who would you recommend us to?%0D%0A%0D%0A Imagine you're speaking to them. How would you describe us to them?%0D%0A%0D%0A  Thanks,  ">click this link</a>!

Please let me know how it goes, and if you have any suggestions. And if you find this useful, you'll <a href="http://signal.ly">love Signally</a> - a Customer Development helper app I'm making based on teaching this stuff in accelerators.

*Update:* I've found a cleaner hack for this - I now ask my customers for intros over email, and ask them to cc me, so I get to hear how they describe me.  I keep these all in a single text file.

&nbsp;

Footnotes

[1] "Imagine you're speaking to them," was a suggestion from a psychologist I met in passing at Leancamp NYC. If  it was you, please let me know so I can credit you.

