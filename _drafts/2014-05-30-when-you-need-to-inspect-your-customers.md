---
layout: post
title:  "When you need to inspect your customers"
date:   2014-05-30 13:13:50
tags:   
---

Low sign up rates, customers slipping away, figuring out pricing, deciding on the right features. These are problems have something in common, more than that they're typical in almost every startup. 

**They're decisions on incomplete information, information about customers that designer techniques are well-equipped to collect.**

If you ask a startup's founder what the startup does, you'll usually get an impressive-sounding one-liner. But when you ask customers, you get a different answer, usually a lot simpler, and a lot more focused what's important to them. When you get a different answer from each, who's correct?

This difference highlights an important problem in a lot of startups, and it goes beyond talking to customers.  It goes to the heart of understanding them in different ways.  

 There are loads of ways to listen:
- what are they typing in search boxes?
- what are they emailing you about?
- what are they asking about when they call you?
what are they saying on social media? what kinds of things are they doing on your site? 




Important point: You want the raw signals, direct from customers, so don't make distil it down into stats and averages, and don't make it anyone else's job to do this. Never outsource your listening.
Averages and metrics filter can out a lot of the important detail. What's more useful: to know what your "average user" does or to know what your top 100 heaviest users do each day? How is that different from the next 200? What can you do with that information?  

How to improve the ad

What were predictors of a booking? descripton length, completeness, quality of photos, time on site

We looked at what questions were being sent to landlords and the responses they gave. but there were 100k listings. rather than sentiment analysis, used random samples and went through reading each text to categorise them so we got a feel for what was important to people. This helped us to help landlords to get more mbokings (customer success). 


##

I see a lot of startups working hard, measuring the right things, but still unable to react in a way that unlocks growth. There's a skill gap to be plugged with product design. 

---

Originally posted at [startups.bg](http://startups.bg/read/when-you-need-to-inspect-your-customers/)

