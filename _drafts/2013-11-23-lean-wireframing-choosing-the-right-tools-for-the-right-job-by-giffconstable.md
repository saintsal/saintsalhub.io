---
layout: post
title:  "Lean Wireframing - choosing the right tools for the right job /by @giffconstable"
date:   2013-11-23 15:13:33
tags:   experiment-design lean-startup
---

<div class="posterous_autopost">
<div class="posterous_bookmarklet_entry">

This is a strong example of truly Lean Thinking (not just Lean Startup Thinking) used in practical design research.
<blockquote class="posterous_long_quote">A wireframe is meant to communicate and test. You want to do the least amount of work required to fulfill those functions. Anything more is a waste of time and resources.

A simple product change might only require a whiteboard or paper sketch before implementation. A more radical or dev-intensive change might justify more concept, design and usability vetting.

When paper testing, let the type of customer dictate your level of fidelity. You can show a techie-early-adopter a fairly crude wireframe and they will get it. However, if your target is a fashionista, you need to have more polish, otherwise the lack of visuals will get in the way of an honest reaction about the core value proposition you are trying to test.

The more you require the customer to use their imagination, the less you can trust their response, but you still want to look for shortcuts to actionable conclusions.

... In the end, it’s not about fidelity at all. The best mockup is one that serves its purpose with the least amount of work and time require.</blockquote>
<div class="posterous_quote_citation">The post goes into <a href="http://giffconstable.com/2011/08/lean-wireframing/">more detail and covers 3 concrete examples. Check it out on giffconstable.com</a></div>
</div>
<p style="font-size: 10px;"><a href="http://posterous.com">Posted via email</a> from <a href="http://saintsal.posterous.com/lean-wireframing-choosing-the-right-tools-for">I'm Sal</a></p>

</div>
