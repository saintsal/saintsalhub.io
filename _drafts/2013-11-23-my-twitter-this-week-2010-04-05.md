---
layout: post
title:  "My Twitter this week: 2010-04-05"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>Settling in for a late night working on @leancamp. @<a href="http://twitter.com/dhh">dhh</a> would reprimand me,  but @<a href="http://twitter.com/garyvee">garyvee</a> would praise me! <a href="http://twitter.com/saintsal/statuses/11384800607">#</a></li>
	<li>RT @garyvee: @<a href="http://twitter.com/SaintSal">SaintSal</a> SUPER PRAISE and dont let @<a href="http://twitter.com/dhh">dhh</a> boss u around ;) – Thanks Gary! I&#39;m sure the Leancampers appreciate it too! <a href="http://twitter.com/saintsal/statuses/11385832635">#</a></li>
	<li>Video: What is Lean Startup? Answered through public verbal abuse at SXSW! #<a href="http://search.twitter.com/search?q=%23leanstartup">leanstartup</a> #<a href="http://search.twitter.com/search?q=%23sxsw">sxsw</a> <a href="http://tumblr.com/xhn83cqty" rel="nofollow">http://tumblr.com/xhn83cqty</a> <a href="http://twitter.com/saintsal/statuses/11434820321">#</a></li>
	<li>Scientist F. Galton used #<a href="http://search.twitter.com/search?q=%23vizthink">vizthink</a> in the 1800s to discover the effect of weather pressure systems.    <a href="http://yfrog.com/9gy21aj" rel="nofollow">http://yfrog.com/9gy21aj</a> <a href="http://twitter.com/saintsal/statuses/11539668224">#</a></li>
</ul>

