---
layout: post
title:  "Lean Information - how to choose the right tool for the information you need."
date:   2014-03-25 19:43:59
tags:   
---

Excited to run this workshop with Spencer Turner of Neo tomorrow!

---

A crowd-sourced day of hands-on techniques to design your business  around your unknowns, with Sal Virani (@saintsal) Partner at Foundercentric and Spencer Turner (@spencerturner) Principal at Neo.  

We often get distracted by learning new tools, and lose sight of when each is actually useful. In an up-beat and conversational worshop, we'll focus on startups and projects that need to find traction. We'll start by asking each participant what challenges they have.

We'll work through some hands-on exercises and activities, giving you real experience with the tools - so you know which will be appropriate in real-word situations.

- when is it actually useful to talk to customers?
- how do we control conversations to get actionable facts, rather than opinion?
- when is iteration less appropriate than parallelisation?
- how do you design studios manage multiple ideas?
- how do help your entire team, and your customers, coalesce around a shared vision?
- how do you choose which customer group to talk to?
- when do you drop your business model idea?
- which dashboard do you use to measure progress?
- how do you really know "the one metric that matters?"
- how can you make better decision with incomplete information?
- how do you kill busy-work, following a process, but not really making progress?
- how do you have a good hypothesis?
- how do choose where to focus?
- how do you recognise the difference between design and ux - what is each really for?

**Bring your current challenges - we'll work on what matters to you!**

---
### About the facilitators 
Sal's seen what really works and doesn't all over the world at Leancamp, and teaches in 20 accelerators. Spencer's worked  with 2-man startups to international banks, to help them better understand what people really want from their products, and build better teams around that.
