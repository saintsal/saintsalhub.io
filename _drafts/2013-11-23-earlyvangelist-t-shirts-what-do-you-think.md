---
layout: post
title:  "Earlyvangelist t-shirts - what do you think?"
date:   2013-11-23 15:13:33
tags:   earlyvangelist leancamp
---

<div class='posterous_autopost'><div class="posterous_bookmarklet_entry"> <div class='p_embed p_image_embed'> <img alt="" src="https://img.skitch.com/20111113-bt34ssq6dcie8em6fceigm7apq.jpg" /> </div> <div class="posterous_quote_citation">via <a href="https://skitch.com/saintsal/ge89p/earlyvangelist-t-shirt">skitch.com</a></div> <p>Thinking about doing some kind of Earlyvangelist t-shirt for swag or fund-raising for Leancamp. What do you think? Worth paying for? Would you wear something like this? (Tell me like it is - I can take the criticism if you think it's a silly idea!)</p></div>      <p style="font-size: 10px;">      <a href="http://posterous.com">Posted via email</a>       from <a href="http://saintsal.posterous.com/earlyvangelist-t-shirts-what-do-you-think">I'm Sal</a>      </p>      </div>
