---
layout: post
title:  "Using FiveSecondTest to quickly spot the right improvements in landing page designs."
date:   2013-11-23 15:13:33
tags:   experiment-design lean-startup
---

I use <a href="http://fivesecondtest.com/" target="_blank">FiveSecondTest.com</a> to test landing pages before I build them. It shows a mockup of your page to random people for 5 seconds, then lets you ask them questions to learn what they recalled and understood. This is very valuable to make sure the message you intended is getting communicated, which is important to rule out if you're smoke testing a value proposition.
<p style="text-align: center;"><strong>It answers the question: "Is this doing poorly because the offer is bad or the design is bad?"</strong></p>
My typical questions are:
<ol>
	<li>What does this page offer?</li>
	<li>What can you do on this site?</li>
	<li>What is the page asking you to do?</li>
	<li>What are some reasons it gives you to do this?</li>
	<li>What do you think will happen when you click the button?</li>
</ol>
You'll notice that these questions aren't leading in any way - they give no clues about the design except for the last question, where I tell them there's a button.

If I don't get the answers I expect to these questions, I know I need to improve the design. You'd be surprised how differently people think from you.  I iterate on FiveSecondTest until people clearly get the point and can answer those questions to my satisfaction. Only then does the page go live.

Sometimes, this is a frustrating process. People are super lazy and don't really read stuff or take a proper look at your page, so you really have spoon feed the information. But sometimes, thanks to the fact that FiveSecondTest has a decent number of smart-ass designers on it, I get answers that crack me up. It's nice when your work makes you laugh out loud.
<a href="https://skitch.com/saintsal/fxw4j/fivesecondtest"><img src="https://img.skitch.com/20110816-bn1cu6whkpqag27pa9xwq1gy7d.preview.jpg" alt="fivesecondtest" /></a>
What are the reasons the page gives you to click the button? A pink arrow. Gee, thanks!

Btw, here's a later version that consistently got the right answers to those questions:
<a href="https://skitch.com/saintsal/fxw65/unbounce-leancamp-membership-preview"><img src="https://img.skitch.com/20110816-fadjr291d7x2461iqymmwbdm2f.preview.jpg" alt="Unbounce - Leancamp membership - Preview" /></a>
One more item and bolder text did it! Go figure.

PS. If you're interested in Leancamp membership, <a href="http://register.leanca.mp/membership/?utm_source=saintsal&amp;utm_medium=blog&amp;utm_campaign=Membership">let me know!</a>
