---
layout: post
title:  "Tweets from the week 2010-01-04"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>@<a href="http://twitter.com/Whatleydude">Whatleydude</a> that&#39;s a shame but it helps to remember digital is ephemeral. We craft flow more than make things. (that said, archive.org!) <a href="http://twitter.com/Whatleydude/statuses/7153761555">in reply to Whatleydude</a> <a href="http://twitter.com/saintsal/statuses/7154063467">#</a></li>
	<li>Considering @<a href="http://twitter.com/pantheon_drupal">pantheon_drupal</a> for my enterprise hosting business - it&#39;s a pre-built, high-performance Drupal server in the Amazon cloud. <a href="http://twitter.com/saintsal/statuses/7159077010">#</a></li>
	<li>Make Your Mockup in Markup (Or How I Learned To Stop Worrying And Love Epicenter Design) /by @<a href="http://twitter.com/meaganfisher">meaganfisher</a> <a href="http://ff.im/-duXBL" rel="nofollow">http://ff.im/-duXBL</a> <a href="http://twitter.com/saintsal/statuses/7163753520">#</a></li>
	<li>Democracy should be the ideal to which anarchistic systems strive.  - Noam Chomsky (On BBC World now.) <a href="http://twitter.com/saintsal/statuses/7176049376">#</a></li>
	<li>@<a href="http://twitter.com/mled">mled</a> @<a href="http://twitter.com/spode">spode</a> Yes, clever &amp; prescient! My wishlist: the tag cloud as a desktop HUD &amp; ability focus it on subject areas that interest me. <a href="http://twitter.com/mled/statuses/7128533525">in reply to mled</a> <a href="http://twitter.com/saintsal/statuses/7176406028">#</a></li>
	<li>@<a href="http://twitter.com/mled">mled</a> Me neither! I meant general desktop HUDs like Growl, Mac OSX widgets or scrolling tickers. Or even as a Seesmic plugin. <a href="http://twitter.com/mled/statuses/7191930324">in reply to mled</a> <a href="http://twitter.com/saintsal/statuses/7196621887">#</a></li>
</ul>

