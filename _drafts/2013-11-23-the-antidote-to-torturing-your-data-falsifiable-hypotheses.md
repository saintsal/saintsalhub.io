---
layout: post
title:  "Torture your data long enough, and it'll tell you anything."
date:   2013-11-23 15:13:33
tags:   experiment-design lean-startup
---

<div style="width:425px" id="__ss_10592745"><strong style="display:block;margin:12px 0 4px"><a href="http://www.slideshare.net/saintsal/torture-your-data-long-enough-itll-tell-you-anything" title="Torture your data long enough, it&#39;ll tell you anything">Torture your data long enough, it&#39;ll tell you anything</a></strong><object id="__sse10592745" width="425" height="355"><param name="movie" value="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=tortureyourdatalongenough-111214115018-phpapp01&stripped_title=torture-your-data-long-enough-itll-tell-you-anything&userName=saintsal" /><param name="allowFullScreen" value="true"/><param name="allowScriptAccess" value="always"/><param name="wmode" value="transparent"/><embed name="__sse10592745" src="http://static.slidesharecdn.com/swf/ssplayer2.swf?doc=tortureyourdatalongenough-111214115018-phpapp01&stripped_title=torture-your-data-long-enough-itll-tell-you-anything&userName=saintsal" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="transparent" width="425" height="355"></embed></object><div style="padding:5px 0 12px">View more <a href="http://www.slideshare.net/">presentations</a> from <a href="http://www.slideshare.net/saintsal">Salim Virani</a>.</div></div>
