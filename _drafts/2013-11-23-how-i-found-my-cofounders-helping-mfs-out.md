---
layout: post
title:  "How I found cofounders that stuck through the hard times"
date:   2013-11-23 15:13:33
tags:   team
---

As our startup ecosystem grows, there are loads of people trying to help match cofounders. These events and online networks will create all kinds of meeting opportunities and surface area for great connections.

<strong>But don't forget what makes the fabric of our ecosystem - helping each other out. That's where real relationships grow.</strong>

Of the 6 businesses I co-founded,  the ones that made it through the tough times started with me helping a friend out. Unfortunately, my help worked. My friends came back for more. Eventually, I had to say, "Look man, I want you to be rich so we can party on your yacht and everything, but if I'm gonna keep this up, you're gonna have to cut me in." So they did.

<strong>Trust usually comes first</strong>

When I look at successful teams, this is a common story. Aligning goals and building trust happens naturally along the way.

<strong>Skipping the relationship building</strong>

These days, Lean Startup means we're getting better at avoiding needless market risk, but it seems more and more founders are putting their blinders on to team risk.

Jordan from GiftCannon recently told his amazing story of market validation and a viral growth engine that looked positive. But the black cloud over his silver lining was that the positive market feedback prompted him to build a team quickly. A few months later, in a flash of lightening, the team had split in two.

<strong>Starting with trust - HAMFO</strong>

<img class="wp-image-3418 alignnone" title="pulp_fiction_stencil_by_shmilblix-d3bgl7s" alt="" src="https://dl.dropboxusercontent.com/u/6606104/www/saintsal/img/2013/04/pulp_fiction_stencil_by_shmilblix-d3bgl7s.png" width="323" height="197" />The only way I know how to address this is from what I know works. HAMFO. Help A Muddling Founder Out.  (When I skip that step, I'm just a silly MF.)

Skipping this is how my most spectacular startup failure happened. A three-way split where I was a friend of one co-founder, but barely knew the other. Seemed like a great market opportunity, and I thought that trust was inductive - that if my friend trusted the third cofounder, then so should I. Had we done something small together, or I had helped him with one of his projects, I would have seen that working together was not viable in the long term.

In every case where I started with helping, the relationships withstood the bumps, and even the failures of the businesses.  And it's not just me, I've heard plenty of stories like this.  One of my favourites is when an experienced founder gives a youngster a shot - a small contract to cut their teeth in their new career - and they grow into the cofounder role.

<strong>Asking for a hand</strong>

One of the unexpected lessons of Rob's <a href="http://www.bootstrapchallenge.com">Bootstrap Challenge</a> project - where he publicly staked £10,000 of his money to get to Ramen Profitability - is that being transparent about his projects and problems attracted offers of help and to join. Now, I think it's a better use of time to help cofounders and partners find you, rather than seeking them.

The ugly flipside of this burger is watching hustling "business" or worse, " idea " founders work the room at a tech meetup, promising that the only obstacle to a multi-million dollar business is if <em>you</em> build <em>their</em> idea. I've been on the receiving end of this - focusing on the skills over the relationship just makes you feel dirty. At least buy me dinner first.

<strong>Taking team risk seriously</strong>

Of course, this is a simplified view. John Mullins of Getting To Plan B fame recently wrote <a href="http://www.newbusinessroadtest.com">The New Business Road Test</a>, which looks at many reasons for startup failure, including a number of team risks, and how to address them early on.

I'd recommend picking it up, especially if you feel you're in market pull, and rushing to build a team. But before you dig in, your future cofounder could be right in front of you. Help a MF out!
