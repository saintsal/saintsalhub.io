---
layout: post
title:  "Big Potatoes"
date:   2013-11-23 15:13:33
tags:   
---

Big Potatoes,a manifesto for risk-taking innovation, is a contribution to improving the climate for innovation globally. It's inspired by Nestlé, Penguin Books, General Electric and Texas Instruments, durable companies that innovated through the Great Dpression, and authored by Norman Lewis, Nico Macdonald, Alan Patrick, Martyn Perks, Mitchell Sava and James Woudhuysen. I advised on digital platforms and online marketing.

<a href="http://bigpotatoes.org">http://bigpotatoes.org</a>
