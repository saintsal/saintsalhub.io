---
layout: post
title:  "Tweets from the week 2009-11-23"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>@<a href="http://twitter.com/ejwc">ejwc</a> True, but the benefits of faster prototypes &amp; always being scalable provide their own kind of flexibility. Another tool for the belt. <a href="http://twitter.com/ejwc/statuses/5782577781">in reply to ejwc</a> <a href="http://twitter.com/saintsal/statuses/5791113573">#</a></li>
	<li>Has anyone found a fast &amp; easy way of building Twitter lists? @<a href="http://twitter.com/cleverclogs">cleverclogs</a> Seen any OPML -&gt; Twitter Lists tools? <a href="http://twitter.com/saintsal/statuses/5791627998">#</a></li>
	<li>@<a href="http://twitter.com/yoni">yoni</a> I&#39;m thinking of doing the same - unless you&#39;re planning on sharing? <a href="http://twitter.com/Yoni/statuses/5793913052">in reply to Yoni</a> <a href="http://twitter.com/saintsal/statuses/5801270235">#</a></li>
	<li>@<a href="http://twitter.com/Yoni">Yoni</a> I&#39;m half-way through a script that builds lists from a textarea full of usernames. Was going to share it with the masses... <a href="http://twitter.com/Yoni/statuses/5801286320">in reply to Yoni</a> <a href="http://twitter.com/saintsal/statuses/5803832308">#</a></li>
	<li>@<a href="http://twitter.com/JoeFernandez">JoeFernandez</a> No, but am keen to check it out.  What&#39;s the link? <a href="http://twitter.com/JoeFernandez/statuses/5803905041">in reply to JoeFernandez</a> <a href="http://twitter.com/saintsal/statuses/5804009508">#</a></li>
	<li>@<a href="http://twitter.com/JoeFernandez">JoeFernandez</a> That&#39;s way better than the Twitter interface! (Request: can it accept multiple names in the text field?) <a href="http://bit.ly/2zjZhb" rel="nofollow">http://bit.ly/2zjZhb</a> <a href="http://twitter.com/JoeFernandez/statuses/5804065357">in reply to JoeFernandez</a> <a href="http://twitter.com/saintsal/statuses/5810365981">#</a></li>
	<li>@<a href="http://twitter.com/JoeFernandez">JoeFernandez</a> I mean more like one textarea where I can dump 40 or 100 names for a list. <a href="http://twitter.com/JoeFernandez/statuses/5810913881">in reply to JoeFernandez</a> <a href="http://twitter.com/saintsal/statuses/5810945082">#</a></li>
	<li>Should the NHS create a World Service? /by @<a href="http://twitter.com/willperrin">willperrin</a> cc @<a href="http://twitter.com/menshealthforum">menshealthforum</a> <a href="http://ff.im/-bDr8N" rel="nofollow">http://ff.im/-bDr8N</a> <a href="http://twitter.com/saintsal/statuses/5821997910">#</a></li>
	<li>What do you do when someone drops a smelly one on your blog? New post on the tasty subject /by @<a href="http://twitter.com/dobronski">dobronski</a> <a href="http://ff.im/-bEjJk" rel="nofollow">http://ff.im/-bEjJk</a> <a href="http://twitter.com/saintsal/statuses/5828702292">#</a></li>
	<li>@<a href="http://twitter.com/tag">tag</a> good to meet you. Hopefully, we&#39;ll bump into @<a href="http://twitter.com/givp">givp</a> to chat about the Turner Gallery. <a href="http://twitter.com/saintsal/statuses/5836017186">#</a></li>
	<li>Just recorded @<a href="http://twitter.com/nickycast">nickycast</a> on offline design patterns RT @clairey_ross: Yay for user experience talk.looking at anti patterns #<a href="http://search.twitter.com/search?q=%23igniteLDN">igniteLDN</a> <a href="http://twitter.com/saintsal/statuses/5837518888">#</a></li>
	<li>What is the sound of one hand clapping? Questions like this are called quans, which harness distraction to focus our minds. #<a href="http://search.twitter.com/search?q=%23zen">zen</a> #<a href="http://search.twitter.com/search?q=%23igniteldn">igniteldn</a> <a href="http://twitter.com/saintsal/statuses/5837638334">#</a></li>
	<li>Doing well + doing good = financially sustainable philanthropy? #<a href="http://search.twitter.com/search?q=%23igniteldn">igniteldn</a> <a href="http://twitter.com/saintsal/statuses/5838766936">#</a></li>
	<li>@<a href="http://twitter.com/givp">givp</a> No we didn&#39;t! @<a href="http://twitter.com/tag">tag</a> was telling me about your work with museums. A few galleries are my consulting clients, so thought we should chat. <a href="http://twitter.com/saintsal/statuses/5874480214">#</a></li>
	<li>@<a href="http://twitter.com/dobronski">dobronski</a> Good to see you too! I&#39;m as entreprenerdy as the JWT days, and a bit hairier. My latest creation is <a href="http://aroundthecorner.tv" rel="nofollow">http://aroundthecorner.tv</a> <a href="http://twitter.com/saintsal/statuses/5874552837">#</a></li>
</ul>

