---
layout: post
title:  "The best questions are the ones that create the most uncertainty."
date:   2013-11-23 15:13:33
tags:   experiment-design
---


These days, we startups are learning from Science and getting comfortable with Uncertainty.  Which is why I find the viewpoint in the video below so helpful - it's a scientist's view about uncertainty and questioning. And not just any scientist, <a href="http://justtrialanderror.com/?p=37">Beau Lotto studies perception</a>!

He explains that we interpret our observations in ways that were useful in the past. We find patterns and create meaning in what we perceive.
<blockquote>
<p style="text-align: center;"><strong>This means our experience distorts our interpretations of what we observe.</strong></p>
</blockquote>
When we find ourselves in a new context, we end up acting in ways that used to work but don't anymore. Sound like any founders you know?

So we need to ask good questions to get our footing again. And the only way to learn something new is to venture into the unknown.

Asking questions about the unknown is easy though. The hard questions,  <a href="http://www.saintsal.com/2012/10/the-question-everyone-asks-but-only-iconoclasts-answer/">the iconoclastic questions</a>, the useful questions, challenge what we already think we know.

Here's where it comes full-circle.

Beau recommends play. And play is also what Alex Osterwalder teaches, who <a href="http://rubenvanderlaan.com/nl/2012/01/we-have-to-think-as-designers-interview-with-alex-osterwalder/">brought design thinking to business models</a>. Beau defines the characteristics of playing – enjoying uncertainty, being cooperative, playing for it's own reward – as the characteristics of a good scientist. And that's what guides good experiments.
<blockquote>"If you add rules to play, you have a game. That's what an experiment is." - Beau Lotto</blockquote>
If you think equating science with play is a stretch, Beau demonstrates this through an experiment with pretty amazing twists, turns, and results. If you're diving into uncertainty, this a great video to watch over a coffee:

<iframe src="http://embed.ted.com/talks/beau_lotto_amy_o_toole_science_is_for_everyone_kids_included.html" height="360" width="640" allowfullscreen="" frameborder="0" scrolling="no"></iframe>
<div class="zemanta-pixie" style="margin-top: 10px; height: 15px;"><a class="zemanta-pixie-a" title="Enhanced by Zemanta" href="http://www.zemanta.com/?px"><img class="zemanta-pixie-img" style="border: none; float: right;" alt="Enhanced by Zemanta" src="http://img.zemanta.com/zemified_e.png?x-id=9bdc2c4f-1d22-4bcb-b14e-e74c42c35da2" /></a></div>
