---
layout: post
title:  "Tweets from the week 2009-12-28"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>Facebook&#39;s $1 Billion Revenues Explained In 25 Words <a href="http://ff.im/-df7OR" rel="nofollow">http://ff.im/-df7OR</a> <a href="http://twitter.com/saintsal/statuses/6931938005">#</a></li>
	<li>@<a href="http://twitter.com/Whatleydude">Whatleydude</a> Tidy Up! <a href="http://twitter.com/Whatleydude/statuses/6936154736">in reply to Whatleydude</a> <a href="http://twitter.com/saintsal/statuses/6936436759">#</a></li>
	<li>@<a href="http://twitter.com/bowbrick">bowbrick</a> Indeed, last minute shoppers tend to be the naughty ones. <a href="http://twitter.com/bowbrick/statuses/6972911407">in reply to bowbrick</a> <a href="http://twitter.com/saintsal/statuses/6973346448">#</a></li>
	<li>If you want to use Google Chrome extensions on Mac OSX, you need to download Chromium, not Chrome. Kind of annoying. <a href="http://twitter.com/saintsal/statuses/6973490926">#</a></li>
	<li>Target check-out game - using gaming design patterns to change real-life behaviour <a href="http://ff.im/-dk87q" rel="nofollow">http://ff.im/-dk87q</a> <a href="http://twitter.com/saintsal/statuses/6997864093">#</a></li>
	<li>Forgot to credit @<a href="http://twitter.com/geoffnorthcott">geoffnorthcott</a> for that last link.  Geoff, is AKQA joining the gaming space? <a href="http://twitter.com/geoffnorthcott/statuses/6997661964">in reply to geoffnorthcott</a> <a href="http://twitter.com/saintsal/statuses/6997944823">#</a></li>
	<li>@<a href="http://twitter.com/geoffnorthcott">geoffnorthcott</a> Wow! Ecodrive is one of the best examples of Audit &amp; Feedback. I didn&#39;t realize it was AKQA - congrats! <a href="http://twitter.com/geoffnorthcott/statuses/6997975540">in reply to geoffnorthcott</a> <a href="http://twitter.com/saintsal/statuses/6998193337">#</a></li>
	<li>@<a href="http://twitter.com/si_lumb">si_lumb</a> I didn&#39;t see that tweet. What was the idea? <a href="http://twitter.com/si_lumb/statuses/6998299311">in reply to si_lumb</a> <a href="http://twitter.com/saintsal/statuses/6998460314">#</a></li>
	<li>@<a href="http://twitter.com/si_lumb">si_lumb</a> That would be fun! Though I can imagine it leading to check-out brawls if people got too competitive! Already a tense context. :) <a href="http://twitter.com/saintsal/statuses/6998813044">#</a></li>
	<li>@<a href="http://twitter.com/geoffnorthcott">geoffnorthcott</a> Quite an interesting trajectory; Ecodrive, then Target. Any other similar projects? What&#39;s next? <a href="http://twitter.com/geoffnorthcott/statuses/6998481703">in reply to geoffnorthcott</a> <a href="http://twitter.com/saintsal/statuses/6999148488">#</a></li>
	<li>Saying yes to the holiday voice in my head: &quot;It&#39;s Christmas, Sal. Stop working and drink riesling!&quot; <a href="http://twitter.com/saintsal/statuses/7005069624">#</a></li>
	<li>Looking for Christmas comedy/talk radio stations while the turkey roasts. Any recommendations? <a href="http://twitter.com/saintsal/statuses/7030154591">#</a></li>
	<li>@<a href="http://twitter.com/semanticwill">semanticwill</a> Whoa, Chrome&#39;s been around.  Probably safer that I&#39;ve moved on to Chromium. <a href="http://twitter.com/semanticwill/statuses/7037152333">in reply to semanticwill</a> <a href="http://twitter.com/saintsal/statuses/7037492127">#</a></li>
	<li>The Economist Will Spend to Get to 500,000 Facebook Fans /via @<a href="http://twitter.com/BBHLabs">BBHLabs</a> <a href="http://ff.im/-dnHU4" rel="nofollow">http://ff.im/-dnHU4</a> <a href="http://twitter.com/saintsal/statuses/7069051983">#</a></li>
	<li>Fascinatingly transparent...Wired vs Wired.com teams airing grievances on a Boing Boing post: <a href="http://bit.ly/18BQoH" rel="nofollow">http://bit.ly/18BQoH</a> /rt @<a href="http://twitter.com/geoffnorthcott">geoffnorthcott</a> <a href="http://twitter.com/saintsal/statuses/7071928811">#</a></li>
	<li>Just tried to fast-forward adverts on live TV.  Real-time has its disadvantages. <a href="http://twitter.com/saintsal/statuses/7072643706">#</a></li>
	<li>@<a href="http://twitter.com/Keidson">Keidson</a> That&#39;s even better than The Third Heat. <a href="http://twitter.com/Keidson/statuses/7075169217">in reply to Keidson</a> <a href="http://twitter.com/saintsal/statuses/7075533325">#</a></li>
	<li>I think it&#39;s just awesome that they have a book &quot;Brain Games for Dummies&quot; /rt @<a href="http://twitter.com/farman51">farman51</a> Was that a Christmas gift? <a href="http://twitter.com/saintsal/statuses/7076615054">#</a></li>
	<li>3AM - I&#39;m learning so much today that I can&#39;t make myself sleep! Thanks @<a href="http://twitter.com/ericries">ericries</a> and @sgblank! No really. <a href="http://twitter.com/saintsal/statuses/7078595328">#</a></li>
	<li>RT @iamdanw: Found a cockney cash machine  <a href="http://twitpic.com/u2mql" rel="nofollow">http://twitpic.com/u2mql</a> <a href="http://twitter.com/saintsal/statuses/7100895369">#</a></li>
	<li>Make augmented reality print-outs that you place on the floor of your empty living room to see how it will look furnished. /RT @<a href="http://twitter.com/Jonas_Forth">Jonas_Forth</a> <a href="http://twitter.com/saintsal/statuses/7101036793">#</a></li>
	<li>@<a href="http://twitter.com/lickbrain">lickbrain</a> If the relationship between advertising &amp; behaviour is an accelerating arms race, we need to be concerned with its direction. <a href="http://twitter.com/lickbrain/statuses/7109804306">in reply to lickbrain</a> <a href="http://twitter.com/saintsal/statuses/7110225093">#</a></li>
	<li>@<a href="http://twitter.com/lickbrain">lickbrain</a> On the slow but effective adaptation mechanisms of buildings, you may be come across &quot;How Buildings Learn&quot; <a href="http://is.gd/5DTrm" rel="nofollow">http://is.gd/5DTrm</a> <a href="http://twitter.com/lickbrain/statuses/7109945302">in reply to lickbrain</a> <a href="http://twitter.com/saintsal/statuses/7110426608">#</a></li>
	<li>Someday I&#39;ll be watching Airplane with my grandchildren &amp; they&#39;ll say &quot;Man I can&#39;t believe they used to let people on planes.&quot; /RT @<a href="http://twitter.com/Mike_FTW">Mike_FTW</a> <a href="http://twitter.com/saintsal/statuses/7120636516">#</a></li>
	<li>Get a real-time cloud view of your Twitter stream with Journotwit by @mled. Nice work. <a href="http://ff.im/-draOu" rel="nofollow">http://ff.im/-draOu</a> <a href="http://twitter.com/saintsal/statuses/7122061916">#</a></li>
	<li>Read It Later or Instapaper? Any advice? <a href="http://twitter.com/saintsal/statuses/7123640306">#</a></li>
	<li>Thanks @<a href="http://twitter.com/konterfai">konterfai</a> &amp; @solle. Yep, I&#39;m an iPhone user.  I read that Instapaper has rendering problems with some sites?  Encountered that? <a href="http://twitter.com/konterfai/statuses/7123718293">in reply to konterfai</a> <a href="http://twitter.com/saintsal/statuses/7124042709">#</a></li>
	<li>Unfinished London - an entertaining history of the Green Belt &amp; Northern Line /by @<a href="http://twitter.com/jayforeman">jayforeman</a> via @<a href="http://twitter.com/stevegarfield">stevegarfield</a> <a href="http://ff.im/-drr5T" rel="nofollow">http://ff.im/-drr5T</a> <a href="http://twitter.com/saintsal/statuses/7124409236">#</a></li>
	<li>5 inflection points in digital signage this year /via @<a href="http://twitter.com/pixmo">pixmo</a> <a href="http://ff.im/-drr5V" rel="nofollow">http://ff.im/-drr5V</a> <a href="http://twitter.com/saintsal/statuses/7124409228">#</a></li>
</ul>

