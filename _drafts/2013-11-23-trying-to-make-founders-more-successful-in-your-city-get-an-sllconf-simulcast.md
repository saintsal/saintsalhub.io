---
layout: post
title:  "Tips on hosting your #sllconf simulcast, and how to use it to strengthen your startup community."
date:   2013-11-23 15:13:33
tags:   community
---

I've organised the <a href="http://sllconf.com">Startup Lessons Learned</a> simulcast in London for the past 2 years.  If you're thinking it would be helpful for your startup community, take up the responsibility and do it. Opportunity gravitates to people who what "somebody aught to do."

Hosting an SLL Conf simulcast is quite easy, and pays off by bringing out smart founders to engage with the community. SLLConf attracted smart people I'd never seen at other startup events, and helped make them part of the ecosystem.

Jonathan Markwell is quoted in <a href="http://www.startuplessonslearned.com/2012/10/host-livestream-of-lean-startup.html">Eric Ries' latest post</a>, saying, <em>"It was fantastic being able to discuss some of the ‘aha moments’ with others late in to the night."</em> This is the trick. You can create an environment around your simulcast that sparks these relationships that lead to strong community.

Here are a few tips:

<strong>Getting started</strong>
<ul>
	<li>It's not about numbers. You'll have a great event with founders who are there to learn, even if it's a few.</li>
	<li>If you're unsure of a venue, start by collecting email addresses from attendees. Email them once the venue is confirmed.</li>
	<li>Email people back as they sign up to get to know them. (I use Rapportive to get a better sense of each person that emails me.) Ask them if they want to help.</li>
	<li>Try to find a space where people have a place to mingle without disrupting people who want to watch the conference. Two rooms or a big hallway is great. A sound barrier can work too, but isn't ideal.</li>
	<li>Don't let getting sponsorship slow you down or get in the way. You can ask people to chip in to cover costs if need-be. I actually prefer this as it makes sure you've got committed attendees, so it's easier to predict attendance. It also means I don't spend time chasing money from sponsors, and can work on the event itself instead.</li>
</ul>
<strong>Food and drinks</strong>
<ul>
	<li>You'll want to organise food. Keep it simple, easy to order, easy to eat with your hands, and easy to clean up. (Pizza, sushi, etc.) With fewer than 30 people, it's easy to get people to chip in for a delivery. For more, you'll want to plan ahead, and include food costs in the ticket price or sponsorship.</li>
	<li>For drinks, an honour bar works great. Buy a load of beer, wine and juices. Have a can saying €1 per drink. When the drinks are low, take the can to the shop and buy more. Easy!</li>
	<li>Bring a kettle, tea, instant coffee and plastic cups.</li>
</ul>
<strong>Technical stuff</strong>
<ul>
	<li>Borrow a backup projector and laptop for streaming. If you're streaming through a cheap router, you'll want a backup router too. (Streaming for 8 hours is tough on equipment.)</li>
	<li>Stream using an ethernet cable. Leave the wifi for the tweeters.</li>
</ul>
<strong>People stuff</strong>
<ul>
	<li>Make introductions.  Since you'll have contact with everyone in advance, you're in the best position to do this and it really helps.</li>
	<li>Remember to enjoy the event, and make it your own. It's your party!</li>
</ul>
Ready? Step 1 is to <a href="http://bit.ly/livestreamform">apply to host</a>.

PS. After SLL Conf, if you'd like to further the Lean Startup community where you are, a great next step is to host a <a href="http://leanca.mp">Leancamp</a>, where founders share practical Lean Startup lessons learned. (If you're interested, start by <a href="http://eepurl.com/i7irj">getting in touch to apply</a> and we'll connect you to our Leancamp Leaders who will work with you. We're scheduling Leancamp around the world for 2013.)
