---
layout: post
title:  "What’s the difference between HTML, XHTML, CSS and PHP?"
date:   2013-11-23 15:13:33
tags:   HTML PHP programming
---

A friend new to web design asked this question, and here’s a short(ish) answer.

HTML is a “markup” language, in that you mark up content with tags. It takes text and adds machine code around it so the browser can make it pretty.

Then XML came along, which looks like HTML in that it’s a markup language, but has nothing to do with making things look good. It’s for structuring data.

At the same time, HTML was getting really ugly because it was trying to handle making things pretty and separating text into meaningful bits like XML, so they came up with CSS. CSS is a way to describe how structred data should be displayed. So the HTML can concentrate on structure, and the CSS on prettiness. When the HTML is only structure, it can be formatted as XML compatible, so that’s XHTML.

PHP is an old-fashioned language in a way. It’s a language that actually does stuff, like calculations or communicating with other servers. This is known as a procedural language. So PHP does the work like calculations and reading databases, and then generates the HTML to be displayed in the browser.
<div><a title="Enhanced by Zemanta" href="http://www.zemanta.com/"><img src="http://img.zemanta.com/zemified_e.png?x-id=7754e3c7-32d0-4c79-be04-cb7db3451fb3" alt="Enhanced by Zemanta" /></a></div>
