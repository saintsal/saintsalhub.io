---
layout: post
title:  "Using social media locally - new place identification system from Yahoo is a major step"
date:   2013-11-23 15:13:33
tags:   
---

<a href="http://developer.yahoo.com/geo/placemaker/guide/" target="_blank">Yahoo Placemaker</a> finds place names in text content, and returns specific location information such as latitude, longitude, other names, political borders, size, etc.

This will be a key tool for developing place-based social media. For example, this will be used to automate a local news feed for a hotel websites and message boards.  News sources selected by management - such as the BBC, Londonist or your preferred local blogs - can be filtered through Placemaker to only show stories that are relevant to your area.

The possibilities are not limited to news either. Any content feed can be examined and filtered, so look for new location-specific services to be designed to use existing content. Also look for existing location-based services, such as review sites, to have much more content on offer.

This is a big step as thus-far, there hasn't been a service that does this type of place identification for free on a mass scale.
