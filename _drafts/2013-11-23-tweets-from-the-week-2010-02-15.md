---
layout: post
title:  "Tweets from the week 2010-02-15"
date:   2013-11-23 15:13:33
tags:   
---

<ul class="aktt_tweet_digest">
	<li>Aha! The editor of that Gormley / Beau Lotto documentary I mentioned has made her entrance on Twitter.  Hello @EditorBron! <a href="http://twitter.com/saintsal/statuses/8912272381">#</a></li>
	<li>Video: Video: Social Enterprise meets #leanstartup? Comments from the first Lean Startup Meetup. <a href="http://tumblr.com/xhn6chyfi" rel="nofollow">http://tumblr.com/xhn6chyfi</a> <a href="http://twitter.com/saintsal/statuses/8963095753">#</a></li>
	<li>@<a href="http://twitter.com/jamieandrews">jamieandrews</a> @<a href="http://twitter.com/mrchrisadams">mrchrisadams</a> I love how they chase you down the street. <a href="http://bit.ly/cXSUcG" rel="nofollow">http://bit.ly/cXSUcG</a> <a href="http://twitter.com/jamieandrews/statuses/8968718710">in reply to jamieandrews</a> <a href="http://twitter.com/saintsal/statuses/8968854034">#</a></li>
	<li>@<a href="http://twitter.com/daveconcannon">daveconcannon</a> I see what you mean - you&#39;ve been RT&#39;d twice now. I&#39;ve stopped it from RTing RTs so that should make it less chatty. <a href="http://twitter.com/daveconcannon/statuses/9017788309">in reply to daveconcannon</a> <a href="http://twitter.com/saintsal/statuses/9018365001">#</a></li>
	<li>Never let @<a href="http://twitter.com/nickycast">nickycast</a> tell you she doesn&#39;t dance. <a href="http://yfrog.com/1dlwjpj" rel="nofollow">http://yfrog.com/1dlwjpj</a> <a href="http://twitter.com/saintsal/statuses/9026937546">#</a></li>
	<li>@<a href="http://twitter.com/brantcooper">brantcooper</a> @<a href="http://twitter.com/miketempleton">miketempleton</a> You might be interested in my list: <a href="http://twitter.com/saintsal/lists/agile-entrepreneurship" rel="nofollow">http://twitter.com/saintsal/lists/agile-entrepreneurship</a> <a href="http://twitter.com/brantcooper/statuses/9068720651">in reply to brantcooper</a> <a href="http://twitter.com/saintsal/statuses/9094355449">#</a></li>
</ul>

