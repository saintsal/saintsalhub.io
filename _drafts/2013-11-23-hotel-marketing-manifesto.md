---
layout: post
title:  "Hotel Marketing Manifesto"
date:   2013-11-23 15:13:33
tags:   
---

<p>Adapting to a changing market is more important than ever for hotels. With the rise in user-generated content &#8211; known as Travel 2.0 &#8211; travel intermediaries, such as Expedia and Tripadvisor, have been seizing control of guest profile information to develop a relationship with customers at the expense of mid range and budget hotels.</p>
<p>To regain control, hotels need to embrace the leading edge of hotel marketing, and recover customer loyalty, by using best practices to stay ahead of competitors and increase revenue. This can be achieved by exploiting the new technology, such as hotel videos and other social media, as well as traditional best practices, such as in-hotel promotion, guest retention and referral marketing.</p>

