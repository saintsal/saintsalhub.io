---
layout: single
title:  "Subscribe"
date:   2015-01-31 18:13:35
tags:   
---

Subscribe to my blog here.  I generally write about my experience with startup decisions, education, and building startup communities. 

<a href="http://saintsal.us4.list-manage1.com/subscribe?u=cd94a6d414d0d34e19cdcf5bd&amp;id=d740a24578">
                                    <i class="icon-envelope"></i>
                                </a>
                    
