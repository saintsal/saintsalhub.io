---
layout: single
title:  "It's great that you found your way here!"
date:   2014-07-30 17:33:47
permalink: gosh
---

I truly appreciate it. My campaign to raise money to help kids get through Cancer was years ago now!

Sorry to say, it hasn't been great news. Since I made that video, I lost my grandfather, grandmother and aunt to cancer. To be honest with you, I'm not over it.

On the plus side, having short hair has done wonders for my professional career, and some child somewhere has my curly hair! [I encourage you to do the same.](https://www.shaveorstyle.org.uk/)

##You actually typed in this link. You're pretty awesome!
Thanks again! You can [donate to Great Ormond Street Hospital here](http://www.gosh.org/gen/donate/). Please do. It helps the kids with the worst cancer cases in the UK. [I've written up a few more reasons why I chose this charity here.](http://saintsal.com/a-good-reason-to-shave-your-head/)
